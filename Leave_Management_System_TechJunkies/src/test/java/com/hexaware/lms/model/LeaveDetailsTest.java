package com.hexaware.lms.model;

import java.time.LocalDate;

import org.junit.Assert;
import org.junit.Test;

import com.hexaware.lms.model.LeaveDetails.LeaveStatus;
import com.hexaware.lms.model.LeaveDetails.LeaveType;

public class LeaveDetailsTest {
	@Test
	public void testId()
	{
		LeaveDetails  e = new LeaveDetails();
		e.setLeaveId(1000);
		Assert.assertEquals(1000, e.getLeaveId());
		
	
	}
	
	@Test
	public void testStartDate()
	{
		LeaveDetails  e = new LeaveDetails();
		e.setStartDate(LocalDate.of(2020, 05, 05));
		Assert.assertEquals(LocalDate.of(2020, 05, 05),e.getStartDate());
		
	
	}
	
	@Test
	public void testEndDate()
	{
		LeaveDetails  e = new LeaveDetails();
		e.setEndDate(LocalDate.of(2020, 05, 05));
		Assert.assertEquals(LocalDate.of(2020, 05, 05),e.getEndDate());
		
	
	}
	@Test
	public void testLeaveReason()
	{
		LeaveDetails  e = new LeaveDetails();
		e.setLeaveReason("Sick");
		Assert.assertEquals("Sick",e.getLeaveReason());
		
	
	}
	
	@Test
	public void testLeaveStatus()
	{
		LeaveDetails  e = new LeaveDetails();
		e.setLeaveStatus(LeaveStatus.PENDING);
		Assert.assertEquals(LeaveStatus.PENDING,e.getLeaveStatus());
		
	
	}

	@Test
	public void testLeaveType()
	{
		LeaveDetails  e = new LeaveDetails();
		e.setLeaveType(LeaveType.OPTIONAL);
		Assert.assertEquals(LeaveType.OPTIONAL,e.getLeaveType());
		
	
	}
	
	@Test
	public void testEmpId()
	{
		LeaveDetails  e = new LeaveDetails();
		e.setEmpId(1000);
		Assert.assertEquals(1000,e.getEmpId());
		
	
	}
	
	 public void testConstructor() {
		 LeaveDetails  e = new LeaveDetails(1000, LocalDate.of(2020, 05, 05), LocalDate.of(2020, 05, 05), "Sick",LeaveStatus.PENDING, LeaveType.CASUAL,1002  );
		 Assert.assertEquals(1000, e.getLeaveId());
		 Assert.assertEquals(LocalDate.of(2020, 05, 05),e.getStartDate());
		 Assert.assertEquals(LocalDate.of(2020, 05, 05),e.getEndDate());
		 Assert.assertEquals("Sick",e.getLeaveReason());
		 Assert.assertEquals(LeaveStatus.PENDING,e.getLeaveStatus());
		 Assert.assertEquals(LeaveType.OPTIONAL,e.getLeaveType());
		 Assert.assertEquals(1000,e.getEmpId());
	 }
	
	
	
	


	
	
	
}
