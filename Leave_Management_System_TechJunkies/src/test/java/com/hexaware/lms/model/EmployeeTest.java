
package com.hexaware.lms.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import java.time.LocalDate;

import org.junit.Assert;
import org.junit.Test;

public class EmployeeTest{
	
	
	@Test
	public void testId()
	{
		Employee  e = new Employee();
		e.setEmpId(1000);
		Assert.assertEquals(1000, e.getEmpId());
		
	
	}
	
	@Test
	public void testName()
	{
		Employee  e = new Employee();
		e.setEmpName("aksh");
		Assert.assertEquals("aksh", e.getEmpName());
		
	
	}
	
	@Test
	public void testDate()
	{
		Employee  e = new Employee();
		e.setEmpDoj(LocalDate.of(2017,05,05));
		Assert.assertEquals( LocalDate.of(2017,05,05), e.getEmpDoj());
		
	
	}
	
	
	
	
	
	@Test
	public void testEmail()
	{
		Employee  e = new Employee();
		e.setEmpEmail("aksh@gmail.com");
		Assert.assertEquals("aksh@gmail.com", e.getEmpEmail());
		
	
	}
	@Test
	public void testManager()
	{
		Employee  e = new Employee();
		e.setManagerId(1000);
		Assert.assertEquals(1000, e.getManagerId());
		
	
	}
	
	@Test
	public void testLeaveBalance()
	{
		Employee  e = new Employee();
		e.setLeaveBalance(1000);
		Assert.assertEquals(1000, e.getLeaveBalance());
		
	
	}
	
	@Test
	public void testLeaveBalance1()
	{
		Employee  e = new Employee();
		e.setLeaveBalance(100);
		Assert.assertNotEquals(1000, e.getLeaveBalance());
		
	
	}
	
	/*@Test
	public void testLeaveBalance2()
	{
		Employee  e = new Employee();
		e.setLeaveBalance(100);
		Assert.assertTrue(1000);
		
	
	}*/
	
	public void constructorTest(){
       Employee e = new Employee(100,"aksh",LocalDate.of(2017,05,05),"aksh@gmail.com",1000,100);
       Assert.assertEquals(1000, e.getEmpId());
       Assert.assertEquals("aksh", e.getEmpName());
       Assert.assertEquals( LocalDate.of(2017,05,05), e.getEmpDoj());
       Assert.assertEquals("aksh@gmail.com", e.getEmpEmail());
       Assert.assertEquals(1000, e.getManagerId());
       Assert.assertNotEquals(1000, e.getLeaveBalance());

	
	}
	
	
}