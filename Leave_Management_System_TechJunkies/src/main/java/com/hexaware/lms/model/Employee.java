package com.hexaware.lms.model;

import java.time.LocalDate;

public class Employee{
     long empId;
     String empName;
     LocalDate empDoj;
     String empEmail;
     long	managerId;
     int leaveBalance;
     public Employee()
     {
    	 
     }
	public Employee(long empId, String empName, LocalDate empDoj, String empEmail, long managerId, int leaveBalance) {
		super();
		this.empId = empId;
		this.empName = empName;
		this.empDoj = empDoj;
		this.empEmail = empEmail;
		this.managerId = managerId;
		this.leaveBalance = leaveBalance;
	}
	public long getEmpId() {
		return empId;
	}
	public void setEmpId(long empId) {
		this.empId = empId;
	}
	public String getEmpName() {
		return empName;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}
	public LocalDate getEmpDoj() {
		return empDoj;
	}
	public void setEmpDoj(LocalDate empDoj) {
		this.empDoj = empDoj;
	}
	public String getEmpEmail() {
		return empEmail;
	}
	public void setEmpEmail(String empEmail) {
		this.empEmail = empEmail;
	}
	public long getManagerId() {
		return managerId;
	}
	public void setManagerId(long managerId) {
		this.managerId = managerId;
	}
	public int getLeaveBalance() {
		return leaveBalance;
	}
	public void setLeaveBalance(int leaveBalance) {
		this.leaveBalance = leaveBalance;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((empDoj == null) ? 0 : empDoj.hashCode());
		result = prime * result + ((empEmail == null) ? 0 : empEmail.hashCode());
		result = prime * result + (int) (empId ^ (empId >>> 32));
		result = prime * result + ((empName == null) ? 0 : empName.hashCode());
		result = prime * result + leaveBalance;
		result = prime * result + (int) (managerId ^ (managerId >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Employee other = (Employee) obj;
		if (empDoj == null) {
			if (other.empDoj != null)
				return false;
		} else if (!empDoj.equals(other.empDoj))
			return false;
		if (empEmail == null) {
			if (other.empEmail != null)
				return false;
		} else if (!empEmail.equals(other.empEmail))
			return false;
		if (empId != other.empId)
			return false;
		if (empName == null) {
			if (other.empName != null)
				return false;
		} else if (!empName.equals(other.empName))
			return false;
		if (leaveBalance != other.leaveBalance)
			return false;
		if (managerId != other.managerId)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Employee [empId=" + empId + ", empName=" + empName + ", empDoj=" + empDoj + ", empEmail=" + empEmail
				+ ", managerId=" + managerId + ", leaveBalance=" + leaveBalance + "]";
	}
	
     
     
}
