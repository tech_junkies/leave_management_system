

package com.hexaware.lms.model;
import java.time.*;
public class LeaveDetails {
	
	long leaveId;
	LocalDate startDate;
	LocalDate endDate;
    String leaveReason;
    LeaveStatus leaveStatus;
    LeaveType	leaveType;
    long	empId;
	
    
    public LeaveDetails(long leaveId, LocalDate startDate, LocalDate endDate, String leaveReason,
			LeaveStatus leaveStatus, LeaveType leaveType, long empId) {
		super();
		this.leaveId = leaveId;
		this.startDate = startDate;
		this.endDate = endDate;
		this.leaveReason = leaveReason;
		this.leaveStatus = leaveStatus;
		this.leaveType = leaveType;
		this.empId = empId;
	}

	public	LeaveDetails() {
    	empId=1098;
    }
	

	public	enum LeaveStatus{

		PENDING,
		APPROVED,
		REJECTED;
	}

	public	enum	LeaveType{
		OPTIONAL,
		SICK,
		CASUAL
	}

	public long getLeaveId() {
		return leaveId;
	}

	public void setLeaveId(long leaveId) {
		this.leaveId = leaveId;
	}

	public LocalDate getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}

	public LocalDate getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}

	public String getLeaveReason() {
		return leaveReason;
	}

	public void setLeaveReason(String leaveReason) {
		this.leaveReason = leaveReason;
	}

	public LeaveStatus getLeaveStatus() {
		return leaveStatus;
	}

	public void setLeaveStatus(LeaveStatus leaveStatus) {
		this.leaveStatus = leaveStatus;
	}

	public LeaveType getLeaveType() {
		return leaveType;
	}

	public void setLeaveType(LeaveType leaveType) {
		this.leaveType = leaveType;
	}

	public long getEmpId() {
		return empId;
	}

	public void setEmpId(long empId) {
		this.empId = empId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (empId ^ (empId >>> 32));
		result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
		result = prime * result + ((leaveType == null) ? 0 : leaveType.hashCode());
		result = prime * result + (int) (leaveId ^ (leaveId >>> 32));
		result = prime * result + ((leaveReason == null) ? 0 : leaveReason.hashCode());
		result = prime * result + ((leaveStatus == null) ? 0 : leaveStatus.hashCode());
		result = prime * result + ((startDate == null) ? 0 : startDate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LeaveDetails other = (LeaveDetails) obj;
		if (empId != other.empId)
			return false;
		if (endDate == null) {
			if (other.endDate != null)
				return false;
		} else if (!endDate.equals(other.endDate))
			return false;
		if (leaveType == null) {
			if (other.leaveType != null)
				return false;
		} else if (!leaveType.equals(other.leaveType))
			return false;
		if (leaveId != other.leaveId)
			return false;
		if (leaveReason == null) {
			if (other.leaveReason != null)
				return false;
		} else if (!leaveReason.equals(other.leaveReason))
			return false;
		if (leaveStatus != other.leaveStatus)
			return false;
		if (startDate == null) {
			if (other.startDate != null)
				return false;
		} else if (!startDate.equals(other.startDate))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "LeaveDetails [leaveId=" + leaveId + ", startDate=" + startDate + ", endDate=" + endDate
				+ ", leaveReason=" + leaveReason + ", leaveStatus=" + leaveStatus + ", leaveType=" + leaveType
				+ ", empId=" + empId + "]";
	}

	
	
}





