package com.hexaware.lms.service;
import com.hexaware.lms.dao.*;
import java.time.LocalDate;
import java.util.Set;
import java.time.Period;

import com.hexaware.lms.model.Employee;
import com.hexaware.lms.model.LeaveDetails;
import com.hexaware.lms.model.LeaveDetails.LeaveStatus;
import com.hexaware.lms.dao.LeaveDetailsDAO;
import com.hexaware.lms.model.*;
public class LeaveDetailsServiceImpl implements	LeaveDetailsService{

	

	@Override
	public LeaveDetails apply(long empId, LeaveDetails leaveDetails) {

		Employee employee=findEmployeeById( empId);
		if(employee!=null) {
			LocalDate startDate = leaveDetails.getStartDate();
			LocalDate endDate = leaveDetails.getEndDate();
			Period	period = Period.between(startDate,endDate);
			int	diff=period.getDays();
			if(checkLeaveBalance(employee)>diff)
			{
				System.out.println("You can apply the leave");
			}
			else
				System.out.println("Not enough leaves");
		}
		else
			System.out.println("Invalid employee");
		return null ;
		
	}



	@Override
	public int checkLeaveBalance(Employee	emp) {
		// TODO Auto-generated method stub
		return emp.getLeaveBalance();
	}

	@Override
	public Employee findEmployeeById(long empId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public LeaveDetails validateLeaveBalance(long empId) {
		// TODO Auto-generated method stub
		return null;
	}

	
	@Override
	public LeaveDetails aprroveOrDeny(long leaveId, LeaveStatus leaveStatus, long managerId, String comments) {
		// TODO Auto-generated method stub
		LeaveDetailsDAO leaveDetailsDao = new	LeaveDetailsDAOImpl();
		LeaveDetails	leaveDetails=leaveDetailsDao.findLeaveById(leaveId);
		Employee	emp=findEmployeeById(managerId);
		if(leaveDetails != null )
		
		{
			if(managerId==emp.getEmpId()) {
				leaveDetails.setLeaveStatus(leaveStatus);
				leaveDetails.setLeaveReason(comments);
			}
			return leaveDetails;
		}
		else
			return null;
	}



	public Set<LeaveDetails> getLeaveBalance(long empId) {
		// TODO Auto-generated method stub
		return null;
	}

	

}
