package com.hexaware.lms.service;
import	com.hexaware.lms.model.*;

import java.util.Set;

import com.hexaware.lms.model.Employee;
import com.hexaware.lms.model.LeaveDetails;
import com.hexaware.lms.model.LeaveDetails.LeaveStatus;

public interface LeaveDetailsService {
	

	Set<LeaveDetails> getLeaveBalance(long empId);

	LeaveDetails	aprroveOrDeny(long leaveId,LeaveStatus leaveStatus,long managerId,String comments);
	
	LeaveDetails	apply(long empId,LeaveDetails	leaveDetails);


	LeaveDetails    validateLeaveBalance(long empId);

	//Set<LeaveDetails> getLeaveBalance(long empId);

	Employee	findEmployeeById(long empId);
	//LeaveDetails    validateLeaveBalance(long empId);

	int checkLeaveBalance(Employee emp);


	
}
