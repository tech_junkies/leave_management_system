package com.hexaware.lms.dao;
import com.hexaware.lms.model.*;
import java.util.Set;

public interface LeaveDetailsDAO {

	//LeaveDetails save(Long	empId, LeaveDetails lv);
	
	//Set <LeaveDetails> findAll(long	leaveId);
	
	//public LeaveDetails findByLeaveId(long	leaveId);

	
	
	
	//Set <LeaveDetails>	findByEmpId(long empID);
	Employee findEmployeeById(long empId);
	void updateLeaveBalance(Employee employee);
	LeaveDetails	findLeaveById(long leaveId);
	

	
	
} 